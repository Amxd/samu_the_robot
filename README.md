#### Project Video
https://youtu.be/F_twfmXoHQw

#### Project Overview
Educational game for young children where they can learn about our Solar system. Children assemble real life puzzles and use a companion app with AR (Augmented Reality) and voice recognition capabilities to learn more about each one of its celestial bodies.
The game is comprised by a set of real life (physical) cardboard puzzles and a companion application. The game play includes assembling a puzzle of a specific celestial body (e.g. the sun, or a planet), after the puzzle is assembled we use the application to recognize the image on the puzzle to interact and learn more about that particular celestial body.

#### Vision
We realize that children's education should be a worldwide concern. Almost (four of them!) all of our team members are fathers, and we believe that technology should have a positive and meaningful role in children's development and that parents should have an active participation in this.

#### Resources used:

* https://www.nasa.gov/mission_pages/hubble/main/index.html
* https://www.nasa.gov/mission_pages/hubble/multimedia/index.html
* https://www.solarsystemscope.com/textures/
* https://www.nasa.gov/image-feature/pluto-global-color-map

#### Technologies used

* Unity
* Blender
* Vuforia
* Watson