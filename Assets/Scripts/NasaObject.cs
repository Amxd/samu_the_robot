﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NasaObject {

    public string Name { get; set; }
    public string ObjectName { get; set; }
    public string[] Facts { get; set; }
}
